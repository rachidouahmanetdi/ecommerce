package com.pfe.ecomerce.service;

import com.pfe.ecomerce.entity.Subscription;
import com.pfe.ecomerce.repository.SubscriptionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SubscriptionService {

    @Autowired
    SubscriptionRepository subscriptionRepository;

    public Subscription savesubscription(Subscription subscription) {

        return subscriptionRepository.save(subscription);
    }
    public List<Subscription> savesubscriptions(List<Subscription> subscription) {

        return subscriptionRepository.saveAll(subscription);
    }

    public List<Subscription> getAllsubscriptions() {

        return subscriptionRepository.findAll();
    }

    public String deletesubscription(Long id) {
        subscriptionRepository.deleteById(id);
        return "deleted";
    }

}
