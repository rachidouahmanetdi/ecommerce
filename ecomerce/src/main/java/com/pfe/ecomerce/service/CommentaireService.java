package com.pfe.ecomerce.service;

import com.pfe.ecomerce.entity.Commentaire;
import com.pfe.ecomerce.repository.CommentaireRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentaireService {
    @Autowired
    CommentaireRepository commentaireRepository;

    public Commentaire saveCommentaire(Commentaire commentaire) {
        return commentaireRepository.save(commentaire);
    }
    public List<Commentaire> saveCommentaires(List<Commentaire> commentaires) {return commentaireRepository.saveAll(commentaires);
    }


    public List<Commentaire> getAllCommentaires() {
        return commentaireRepository.findAll();
    }

    public String deleteCommentaire(Long id) {
        commentaireRepository.deleteById(id);
        return "deleted";
    }
}
