package com.pfe.ecomerce.service;

import com.pfe.ecomerce.entity.Product;
import com.pfe.ecomerce.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public Product saveProduct(Product product) {
        return productRepository.save(product);
    }

    public List<Product> saveProducts(List<Product> products) {

        return productRepository.saveAll(products);
    }

    public List<Product> getAllProducts() {

        return productRepository.findAll();
    }

    public String deleteProduct(Long id) {
        productRepository.deleteById(id);
        return "deleted";
    }
}
