package com.pfe.ecomerce.service;

import com.pfe.ecomerce.entity.Company;
import com.pfe.ecomerce.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService {

    @Autowired
    CompanyRepository companyRepository;

    public Company saveCompany(Company company) {
        return companyRepository.save(company);
    }
    public List<Company> saveCompanys(List<Company> companys) {
        return companyRepository.saveAll(companys);
    }


    public List<Company> getAllCompanys() {
        return companyRepository.findAll();
    }

    public String deleteCompany(Long id) {
        companyRepository.deleteById(id);
        return "deleted";
    }
}
