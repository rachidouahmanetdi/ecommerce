package com.pfe.ecomerce.service;

import com.pfe.ecomerce.entity.ItemWishList;
import com.pfe.ecomerce.repository.ItemWishListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ItemWishListService {
    @Autowired
    ItemWishListRepository ItemWhishListRepository;

    public ItemWishList savetemWhishlist(ItemWishList itemwhishlist) {
        return ItemWhishListRepository.save(itemwhishlist);
    }

    public List<ItemWishList> savetemWhishlists(List<ItemWishList> itemwhishlists) {

        return ItemWhishListRepository.saveAll(itemwhishlists);
    }

    public List<ItemWishList> getAllItemWhishlists() {

        return ItemWhishListRepository.findAll();
    }

    public String deletetemWhishlist(Long id) {
        ItemWhishListRepository.deleteById(id);
        return "deleted";
    }
}
