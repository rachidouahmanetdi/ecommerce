package com.pfe.ecomerce.service;

import org.springframework.stereotype.Service;
import com.pfe.ecomerce.entity.Command;
import com.pfe.ecomerce.repository.CommandRepository;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;
@Service
public class CommandService{
    @Autowired
    CommandRepository commandRepository;

    public Command saveCommand(Command command) {
        return commandRepository.save(command);
    }
    public List<Command> saveCommand(List<Command> commands) {
        return commandRepository.saveAll(commands);
    }


    public List<Command> getAllCommands() {
        return commandRepository.findAll();
    }

    public String deleteCommand(Long id) {
        commandRepository.deleteById(id);
        return "deleted";
    }

}
