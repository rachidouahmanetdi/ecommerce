package com.pfe.ecomerce.service;

import com.pfe.ecomerce.dto.UserDTO;
import com.pfe.ecomerce.entity.User;
import com.pfe.ecomerce.enums.Role;
import com.pfe.ecomerce.exception.AlreadyExistException;
import com.pfe.ecomerce.exception.NotFoundException;
import com.pfe.ecomerce.exception.NullValueException;
import com.pfe.ecomerce.exception.WrongPasswordException;
import com.pfe.ecomerce.mapper.UserMapper;
import com.pfe.ecomerce.repository.UserRepository;
import com.pfe.ecomerce.security.JwtTokenProvider;
import com.pfe.ecomerce.util.email.EmailHtmlSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.thymeleaf.context.Context;


import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    protected PasswordEncoder passwordEncoder;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired(required=true)
    private UserMapper userMapper;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private EmailHtmlSender emailHtmlSender;

    public User saveUser(User user) {
        return userRepository.save(user);
    }

    public List<User> saveUsers(List<User> users) {

        return userRepository.saveAll(users);
    }

    public List<User> getAllUsers() {

        return userRepository.findAll();
    }

    public String deleteUser(Long id) {
        userRepository.deleteById(id);
        return "deleted";
    }

    @Transactional
    public UserDTO addUser(UserDTO userDTO) {


       if (userRepository.findByEmail(userDTO.getEmail()).isPresent()) {
           throw new AlreadyExistException("email");
      }
     if (userRepository.findByEmail(userDTO.getEmail()).isPresent()) {
          throw new AlreadyExistException("nom utilisateur");
     }

        String regex = "^(.+)@(.+)$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(userDTO.getEmail());
        if(!matcher.matches()){
           // throw new WrongEmailException();
            throw new IllegalArgumentException();
        }

        String password=userDTO.getPassword();
        userDTO.setPassword( passwordEncoder.encode(password));
        userDTO.setRole(Role.ROLE_USER);
        User utilisateur = userRepository.save(userMapper.mapToEntity(userDTO));

        sendMail(userDTO.getEmail(),userDTO.getEmail(),password);

        return userMapper.mapToDTO(utilisateur);
    }
    @Transactional
    public UserDTO authenticate(UserDTO userDTO) {
       if (userDTO == null) {
            throw new NullValueException("user");
       }
        if (userDTO.getEmail() == null) {
          throw new NullValueException("email");
        }
       if (userDTO.getPassword() == null) {
         throw new NullValueException("password");
      }
        String username = userDTO.getEmail().toLowerCase();
        User userDb = findByEmailUser(username);

        if (!passwordEncoder.matches(userDTO.getPassword(), userDb.getPassword())) {
            throw new WrongPasswordException(User.class);

        }

        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, userDTO.getPassword()));



        UserDTO userDto = userMapper.mapToDTO(userDb);

        userDto.setToken(jwtTokenProvider.createToken(username, findByEmailUser(username).getRole()));

        return userDto;
    }

    @Transactional(readOnly = true)
    public User findByEmailUser(String email) throws IllegalArgumentException {
        if (email == null) {
            throw new NullValueException(email);
        }
        User user = userRepository.findByEmail(email)
                .orElseThrow(() -> new NotFoundException(email));
        return user;
    }

    private void sendMail(String email,String username,String password) {
        final Context context = new Context();
        context.setVariable("password", password);
        context.setVariable("username", username);
        emailHtmlSender.send(email, "nouveau compte",
                "email/email.html", context);
    }
}
