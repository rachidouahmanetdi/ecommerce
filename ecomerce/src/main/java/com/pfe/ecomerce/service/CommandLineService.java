package com.pfe.ecomerce.service;

import com.pfe.ecomerce.entity.CommandLine;
import com.pfe.ecomerce.repository.CommandLineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommandLineService {
    @Autowired
    CommandLineRepository commandlineRepository;

    public CommandLine saveCommandLine(CommandLine commandline) {return commandlineRepository.save(commandline);
    }
    public List<CommandLine> saveCommandLines(List<CommandLine> commandlines) {return commandlineRepository.saveAll(commandlines);
    }


    public List<CommandLine> getAllCommandLines() {
        return commandlineRepository.findAll();
    }

    public String deleteCommandLine(Long id) {
        commandlineRepository.deleteById(id);
        return "deleted";
    }
}
