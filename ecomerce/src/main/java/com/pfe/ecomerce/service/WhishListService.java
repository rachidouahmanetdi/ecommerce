package com.pfe.ecomerce.service;


import com.pfe.ecomerce.entity.WishList;
import com.pfe.ecomerce.repository.WishListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WhishListService {
    @Autowired
    WishListRepository wishListRepository;

    public WishList saveWishList(WishList wishlist) {

        return wishListRepository.save(wishlist);
    }
    public List<WishList> saveWhishLists(List<WishList> wishLists) {

        return wishListRepository.saveAll(wishLists);
    }

    public List<WishList> getAllWhishLists() {

        return wishListRepository.findAll();
    }

    public String deleteWhishLists(Long id) {
        wishListRepository.deleteById(id);
        return "deleted";
    }
}
