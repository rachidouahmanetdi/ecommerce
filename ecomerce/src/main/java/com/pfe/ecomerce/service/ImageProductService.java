package com.pfe.ecomerce.service;

import com.pfe.ecomerce.entity.ImageProduct;
import com.pfe.ecomerce.repository.ImageProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ImageProductService {
    @Autowired
    ImageProductRepository imgproductRepository;

    public ImageProduct saveImageProduct(ImageProduct imgproduct) {
        return imgproductRepository.save(imgproduct);
    }

    public List<ImageProduct> saveImageProducts(List<ImageProduct> imgproducts) {

        return imgproductRepository.saveAll(imgproducts);
    }

    public List<ImageProduct> getAllImageProducts() {

        return imgproductRepository.findAll();
    }

    public String deleteImageProduct(Long id) {
        imgproductRepository.deleteById(id);
        return "deleted";
    }
}
