package com.pfe.ecomerce.service;

import com.pfe.ecomerce.entity.Shipement;
import com.pfe.ecomerce.repository.ShipementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ShipementService {

    @Autowired
    ShipementRepository shipementRepository;

    public Shipement saveShipement(Shipement shipement) {
        return shipementRepository.save(shipement);
    }

    public List<Shipement> saveShipements(List<Shipement> shipements) {

        return shipementRepository.saveAll(shipements);
    }

    public List<Shipement> getAllShipement() {

        return shipementRepository.findAll();
    }

    public String deleteShipement(Long id) {
        shipementRepository.deleteById(id);
        return "deleted";
    }

}
