package com.pfe.ecomerce.service;

import com.pfe.ecomerce.entity.Basket;
import com.pfe.ecomerce.repository.BasketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BasketService {
   @Autowired
    BasketRepository basketRepository;

    public Basket saveBasket(Basket basket) {
        return basketRepository.save(basket);
    }
    public List<Basket> saveBaskets(List<Basket> baskets) {return basketRepository.saveAll(baskets);
    }


    public List<Basket> getAllBaskets() {
        return basketRepository.findAll();
    }

    public String deleteBasket(Long id) {
        basketRepository.deleteById(id);
        return "deleted";
    }

}
