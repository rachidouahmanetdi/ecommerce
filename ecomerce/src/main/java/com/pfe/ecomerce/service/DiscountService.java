package com.pfe.ecomerce.service;

import com.pfe.ecomerce.entity.Discount;
import com.pfe.ecomerce.repository.DiscountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DiscountService {

    @Autowired
    DiscountRepository discountRepository;

    public Discount saveDiscount(Discount discount) {
        return discountRepository.save(discount);
    }
    public List<Discount> saveDiscounts(List<Discount> discounts) {
        return discountRepository.saveAll(discounts);
    }

    public List<Discount> getAllDiscounts() {
        return discountRepository.findAll();
    }

    public String deleteDiscount(Long id) {
        discountRepository.deleteById(id);
        return "deleted";
    }
}
