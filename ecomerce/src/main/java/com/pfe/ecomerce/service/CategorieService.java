package com.pfe.ecomerce.service;

import com.pfe.ecomerce.entity.Categorie;
import com.pfe.ecomerce.repository.CategorieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class CategorieService {
    @Autowired
    CategorieRepository categorieRepository;
    public Categorie saveCategorie(Categorie categorie){return categorieRepository.save(categorie);}
    public List<Categorie> saveCategories(List<Categorie> categories){return categorieRepository.saveAll(categories);}

    public List<Categorie> getAllCategories(){return categorieRepository.findAll();}
    public String deleteCategorie(Long id){
        categorieRepository.deleteById(id);
        return "deleted";
    }











}
