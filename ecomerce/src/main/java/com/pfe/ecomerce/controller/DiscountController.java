package com.pfe.ecomerce.controller;
import com.pfe.ecomerce.entity.Discount;
import com.pfe.ecomerce.service.DiscountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/discount")
public class DiscountController {
    //injection de service
    @Autowired
    DiscountService discountService;

    @GetMapping
    public ResponseEntity<List<Discount>> getAlldiscounts()
    {
        return ResponseEntity.status(HttpStatus.OK).body(discountService.getAllDiscounts());
    }
    //creation de web service de type post

    @PostMapping
    public ResponseEntity<Discount> saveDiscountList(@RequestBody Discount discount)
    {
        return ResponseEntity.status(HttpStatus.CREATED).body(discountService.saveDiscount(discount));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable Long id){
        return ResponseEntity.status(HttpStatus.OK).body(discountService.deleteDiscount(id));
    }
}
