package com.pfe.ecomerce.controller;

import com.pfe.ecomerce.entity.Categorie;
import com.pfe.ecomerce.service.CategorieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@Controller
@RequestMapping("/categorie")
public class CategorieController {
    //injection de service
    @Autowired
    CategorieService categorieService;

    @GetMapping
    public ResponseEntity<List<Categorie>> getAllcategories()
    {
        return ResponseEntity.status(HttpStatus.OK).body(categorieService.getAllCategories());
    }
    //creation de web service de type post

    @PostMapping
    public ResponseEntity<Categorie> saveCategorieList(@RequestBody Categorie categorie)
    {
        return ResponseEntity.status(HttpStatus.CREATED).body(categorieService.saveCategorie(categorie));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteCategorie(@PathVariable Long id){
        return ResponseEntity.status(HttpStatus.OK).body(categorieService.deleteCategorie(id));
    }
}
