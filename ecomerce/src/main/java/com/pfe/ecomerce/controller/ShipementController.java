package com.pfe.ecomerce.controller;

import com.pfe.ecomerce.entity.Shipement;
import com.pfe.ecomerce.service.ShipementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/shipement")
public class ShipementController {

    //injection de service
    @Autowired
    ShipementService shipementService;

    @GetMapping
    public ResponseEntity<List<Shipement>> getAllshipements()
    {
        return ResponseEntity.status(HttpStatus.OK).body(shipementService.getAllShipement());
    }
    //creation de web service de type post

    @PostMapping
    public ResponseEntity<Shipement> saveShipementList(@RequestBody Shipement shipement)
    {
        return ResponseEntity.status(HttpStatus.CREATED).body(shipementService.saveShipement(shipement));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteShipement(@PathVariable Long id){
        return ResponseEntity.status(HttpStatus.OK).body(shipementService.deleteShipement(id));
    }
}
