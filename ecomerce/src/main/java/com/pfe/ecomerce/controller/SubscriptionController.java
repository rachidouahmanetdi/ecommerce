package com.pfe.ecomerce.controller;

import com.pfe.ecomerce.entity.Subscription;
import com.pfe.ecomerce.service.SubscriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/subscription")
public class SubscriptionController {

    @Autowired
    SubscriptionService subscriptionService;

    @GetMapping
    public ResponseEntity<List<Subscription>> getAllSubscriptions()
    {
        return ResponseEntity.status(HttpStatus.OK).body(subscriptionService.getAllsubscriptions());
    }
    //creation de web service de type post

    @PostMapping
    public ResponseEntity<Subscription> saveSubscriptionList(@RequestBody Subscription subscription)
    {
        return ResponseEntity.status(HttpStatus.CREATED).body(subscriptionService.savesubscription(subscription));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteSubscription(@PathVariable Long id){
        return ResponseEntity.status(HttpStatus.OK).body(subscriptionService.deletesubscription(id));
    }
}
