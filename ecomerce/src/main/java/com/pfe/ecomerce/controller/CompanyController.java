package com.pfe.ecomerce.controller;
import com.pfe.ecomerce.entity.Company;
import com.pfe.ecomerce.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/company")
public class CompanyController {
    //injection de service
    @Autowired
    CompanyService companyService;

    @GetMapping
    public ResponseEntity<List<Company>> getAllcompanys()
    {
        return ResponseEntity.status(HttpStatus.OK).body(companyService.getAllCompanys());
    }
    //creation de web service de type post

    @PostMapping
    public ResponseEntity<Company> saveCompanyList(@RequestBody Company company)
    {
        return ResponseEntity.status(HttpStatus.CREATED).body(companyService.saveCompany(company));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteCompany(@PathVariable Long id){
        return ResponseEntity.status(HttpStatus.OK).body(companyService.deleteCompany(id));
    }

}
