package com.pfe.ecomerce.controller;

import com.pfe.ecomerce.entity.Product;
import com.pfe.ecomerce.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/product")
public class ProductController {
    //injection de service
    @Autowired
    ProductService productService;

    @GetMapping
    public ResponseEntity<List<Product>> getAllProducts()
    {
        return ResponseEntity.status(HttpStatus.OK).body(productService.getAllProducts());
    }
    //creation de web service de type post

    @PostMapping
    public ResponseEntity<Product> saveProductList(@RequestBody Product product)
    {
        return ResponseEntity.status(HttpStatus.CREATED).body(productService.saveProduct(product));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable Long id){
        return ResponseEntity.status(HttpStatus.OK).body(productService.deleteProduct(id));
    }

}
