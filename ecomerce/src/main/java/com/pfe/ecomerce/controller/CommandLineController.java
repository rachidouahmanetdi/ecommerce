package com.pfe.ecomerce.controller;
import com.pfe.ecomerce.entity.CommandLine;
import com.pfe.ecomerce.service.CommandLineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/commandline")
public class CommandLineController {
    //injection de service
    @Autowired
    CommandLineService commandlineservice;

    @GetMapping
    public ResponseEntity<List<CommandLine>> getAllcommandlines()
    {
        return ResponseEntity.status(HttpStatus.OK).body(commandlineservice.getAllCommandLines());
    }
    //creation de web service de type post

    @PostMapping
    public ResponseEntity<CommandLine> saveCommandLineList(@RequestBody CommandLine commandline)
    {
        return ResponseEntity.status(HttpStatus.CREATED).body(commandlineservice.saveCommandLine(commandline));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteCommandLine(@PathVariable Long id){
        return ResponseEntity.status(HttpStatus.OK).body(commandlineservice.deleteCommandLine(id));
    }
}
