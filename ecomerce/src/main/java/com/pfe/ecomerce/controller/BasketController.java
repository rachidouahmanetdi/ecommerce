package com.pfe.ecomerce.controller;

import com.pfe.ecomerce.entity.Basket;
import com.pfe.ecomerce.service.BasketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/basket")
public class BasketController {
    //injection de service
    @Autowired
    BasketService basketService;

    @GetMapping
    public ResponseEntity<List<Basket>> getAllbaskets()
    {
        return ResponseEntity.status(HttpStatus.OK).body(basketService.getAllBaskets());
    }
    //creation de web service de type post

    @PostMapping
    public ResponseEntity<Basket> saveBasketList(@RequestBody Basket basket)
    {
        return ResponseEntity.status(HttpStatus.CREATED).body(basketService.saveBasket(basket));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteBasket(@PathVariable Long id){
        return ResponseEntity.status(HttpStatus.OK).body(basketService.deleteBasket(id));
    }
}
