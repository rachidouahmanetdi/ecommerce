package com.pfe.ecomerce.controller;


import com.pfe.ecomerce.entity.WishList;
import com.pfe.ecomerce.service.WhishListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/whishlist")
public class WhishListController {

    //injection de service
    @Autowired
    WhishListService whishlistService;

    @GetMapping
    public ResponseEntity<List<WishList>> getAllwhishlists()
    {
        return ResponseEntity.status(HttpStatus.OK).body(whishlistService.getAllWhishLists());
    }
    //creation de web service de type post

    @PostMapping
    public ResponseEntity<WishList> saveWishList(@RequestBody WishList whishlist)
    {
        return ResponseEntity.status(HttpStatus.CREATED).body(whishlistService.saveWishList(whishlist));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteWhishList(@PathVariable Long id){
        return ResponseEntity.status(HttpStatus.OK).body(whishlistService.deleteWhishLists(id));
    }

}
