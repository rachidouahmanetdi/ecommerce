package com.pfe.ecomerce.controller;

import com.pfe.ecomerce.entity.Command;
import com.pfe.ecomerce.service.CommandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/command")
public class CommandController {
    //injection de service
    @Autowired
    CommandService commandService;

    @GetMapping
    public ResponseEntity<List<Command>> getAllcommands()
    {
        return ResponseEntity.status(HttpStatus.OK).body(commandService.getAllCommands());
    }
    //creation de web service de type post

    @PostMapping
    public ResponseEntity<Command> saveCommandList(@RequestBody Command command)
    {
        return ResponseEntity.status(HttpStatus.CREATED).body(commandService.saveCommand(command));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteCommand(@PathVariable Long id){
        return ResponseEntity.status(HttpStatus.OK).body(commandService.deleteCommand(id));
    }
}
