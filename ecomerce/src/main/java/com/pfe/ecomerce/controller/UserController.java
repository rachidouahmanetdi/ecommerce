package com.pfe.ecomerce.controller;

import com.pfe.ecomerce.dto.UserDTO;
import com.pfe.ecomerce.entity.User;
import com.pfe.ecomerce.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {
    //injection de service
    @Autowired
    UserService userService;

    @GetMapping
    public ResponseEntity<List<User>> getAllusers()
    {
        return ResponseEntity.status(HttpStatus.OK).body(userService.getAllUsers());
    }
    //creation de web service de type post

//    @PostMapping
//    public ResponseEntity<User> saveUserList(@RequestBody User user)
//    {
//        return ResponseEntity.status(HttpStatus.CREATED).body(userService.saveUser(user));
//    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable Long id){
        return ResponseEntity.status(HttpStatus.OK).body(userService.deleteUser(id));
    }

    @PostMapping(value = "/authenticate")
    public ResponseEntity<UserDTO> authenticate(
            @RequestBody UserDTO user) {
        return ResponseEntity.status(HttpStatus.OK).body(userService.authenticate(user));
    }


    @PostMapping
    public ResponseEntity<UserDTO> addUser(@RequestBody UserDTO user) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(userService.addUser(user));
    }
}
