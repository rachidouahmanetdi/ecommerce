package com.pfe.ecomerce.controller;
import com.pfe.ecomerce.entity.Commentaire;
import com.pfe.ecomerce.service.CommentaireService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/commentaire")
public class CommentaireController {
    //injection de service
    @Autowired
    CommentaireService commentaireService;

    @GetMapping
    public ResponseEntity<List<Commentaire>> getAllcommentaires()
    {
        return ResponseEntity.status(HttpStatus.OK).body(commentaireService.getAllCommentaires());
    }
    //creation de web service de type post

    @PostMapping
    public ResponseEntity<Commentaire> saveCommentaireList(@RequestBody Commentaire commentaire)
    {
        return ResponseEntity.status(HttpStatus.CREATED).body(commentaireService.saveCommentaire(commentaire));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteCommentaire(@PathVariable Long id){
        return ResponseEntity.status(HttpStatus.OK).body(commentaireService.deleteCommentaire(id));
    }
}
