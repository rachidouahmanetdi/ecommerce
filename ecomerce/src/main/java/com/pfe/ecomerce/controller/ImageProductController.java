package com.pfe.ecomerce.controller;

import com.pfe.ecomerce.entity.ImageProduct;
import com.pfe.ecomerce.service.ImageProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/imageproduct")
public class ImageProductController {

    //injection de service
    @Autowired
    ImageProductService imgProductService;

    @GetMapping
    public ResponseEntity<List<ImageProduct>> getAllImagesProduct()
    {
        return ResponseEntity.status(HttpStatus.OK).body(imgProductService.getAllImageProducts());
    }
    //creation de web service de type post

    @PostMapping
    public ResponseEntity<ImageProduct> saveImageProductList(@RequestBody ImageProduct imgproduct)
    {
        return ResponseEntity.status(HttpStatus.CREATED).body(imgProductService.saveImageProduct(imgproduct));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteImageProduct(@PathVariable Long id){
        return ResponseEntity.status(HttpStatus.OK).body(imgProductService.deleteImageProduct(id));
    }
}
