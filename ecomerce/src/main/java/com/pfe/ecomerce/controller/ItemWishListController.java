package com.pfe.ecomerce.controller;

import com.pfe.ecomerce.entity.ItemWishList;
import com.pfe.ecomerce.service.ItemWishListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/itemwishList")
public class ItemWishListController {

    //injection de service
    @Autowired
    ItemWishListService itemwishListService;

    @GetMapping
    public ResponseEntity<List<ItemWishList>> getAllitemWishLists()
    {
        return ResponseEntity.status(HttpStatus.OK).body(itemwishListService.getAllItemWhishlists());
    }
    //creation de web service de type post

    @PostMapping
    public ResponseEntity<ItemWishList> saveItemWishListList(@RequestBody ItemWishList itemwishList)
    {
        return ResponseEntity.status(HttpStatus.CREATED).body(itemwishListService.savetemWhishlist(itemwishList));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteItemWishList(@PathVariable Long id){
        return ResponseEntity.status(HttpStatus.OK).body(itemwishListService.deletetemWhishlist(id));
    }
}
