package com.pfe.ecomerce.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pfe.ecomerce.enums.Role;
import lombok.*;

import javax.persistence.Column;


@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {
    private String firstName;

    private String lastName;

    private String adresse;

    private String email;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    private String phone;

    private Role role;

    private String token;

}
