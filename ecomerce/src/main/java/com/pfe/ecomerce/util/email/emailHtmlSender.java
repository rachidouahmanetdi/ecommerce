package com.pfe.ecomerce.util.email;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Service
@RequiredArgsConstructor
public class EmailHtmlSender {

    @Autowired
    private EmailSender emailSender;
    @Autowired
    private TemplateEngine templateEngine;

    @Value("${spring.mail.public-url}")
    private String publicUrl;
    @Value("${spring.mail.platform-name}")
    private String platformName;


    @Async
    public void send(String to, String subject, String templateName, Context context) {
        context.setVariable("publicUrl", publicUrl);
        context.setVariable("platformName", platformName);
        String body = templateEngine.process(templateName, context);
        emailSender.sendHtml(to, subject, body);
    }
}
