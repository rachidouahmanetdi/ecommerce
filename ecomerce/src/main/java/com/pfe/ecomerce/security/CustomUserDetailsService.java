package com.pfe.ecomerce.security;


import com.pfe.ecomerce.entity.User;
import com.pfe.ecomerce.exception.NotFoundException;
import com.pfe.ecomerce.repository.UserRepository;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;


@Component
@Primary
public class CustomUserDetailsService implements UserDetailsService {
    private UserRepository userRepository;

    public CustomUserDetailsService(UserRepository users) {
        this.userRepository = users;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws NotFoundException {
        return userRepository.findByEmail(username).orElseThrow(()
                -> new NotFoundException(User.class, username));
    }
}
