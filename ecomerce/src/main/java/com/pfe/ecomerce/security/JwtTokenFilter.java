package com.pfe.ecomerce.security;

import com.pfe.ecomerce.dto.error.RestError;
import com.pfe.ecomerce.exception.FunctionalErrorCode;
import com.pfe.ecomerce.exception.InvalidJwtAuthenticationException;
import com.pfe.ecomerce.service.UserService;
import com.pfe.ecomerce.util.MapperUtils;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED;

public class JwtTokenFilter extends GenericFilterBean {
    private JwtTokenProvider jwtTokenProvider;
    private UserService userService;

    public JwtTokenFilter(JwtTokenProvider jwtTokenProvider, UserService userService) {
        this.jwtTokenProvider = jwtTokenProvider;
        this.userService = userService;
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain filterChain)
            throws IOException, ServletException {
        try {
            String token = jwtTokenProvider.resolveToken((HttpServletRequest) req);
            if (token != null && jwtTokenProvider.validateToken(token)) {
                Authentication auth = jwtTokenProvider.getAuthentication(token);
                SecurityContextHolder.getContext().setAuthentication(auth);


            }
        } catch (InvalidJwtAuthenticationException e) {
            HttpServletResponse httpServletResponse = (HttpServletResponse) res;

            httpServletResponse.setStatus(SC_UNAUTHORIZED);
            httpServletResponse.setHeader("content-type", MediaType.APPLICATION_JSON_VALUE);
            httpServletResponse.getWriter().print(MapperUtils.mapToJson(RestError.builder()
                    .code(FunctionalErrorCode.INVALID_JWT_TOKEN.getCode())
                    .message(e.getMessage())
                    .build()) );

            return;
        }
        filterChain.doFilter(req, res);
    }
}
