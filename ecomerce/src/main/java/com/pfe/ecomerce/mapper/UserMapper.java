package com.pfe.ecomerce.mapper;

import com.pfe.ecomerce.dto.UserDTO;
import com.pfe.ecomerce.entity.User;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(config = MapStructConfig.class)
public interface UserMapper {

    User mapToEntity(UserDTO userDTO);

    UserDTO mapToDTO(User user);

    List<User> mapToListOfEntity(List<UserDTO> userDTOList);

    List<UserDTO> mapToListOfDTO(List<User> userList);


}

