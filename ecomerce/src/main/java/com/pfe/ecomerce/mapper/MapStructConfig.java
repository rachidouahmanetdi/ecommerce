package com.pfe.ecomerce.mapper;

import org.mapstruct.MapperConfig;

@MapperConfig(componentModel = "spring")
public interface MapStructConfig {
}
