package com.pfe.ecomerce.repository;

import com.pfe.ecomerce.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {
    @Override
    Optional<User> findById(Long id);
    Optional<User> findByEmail(String email);

}
