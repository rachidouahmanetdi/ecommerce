package com.pfe.ecomerce.repository;

import com.pfe.ecomerce.entity.ItemWishList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ItemWishListRepository extends JpaRepository<ItemWishList, Long>, JpaSpecificationExecutor<ItemWishList> {
}
