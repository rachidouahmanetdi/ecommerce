package com.pfe.ecomerce.repository;
import com.pfe.ecomerce.entity.Command;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
public interface CommandRepository extends JpaRepository<Command, Long>, JpaSpecificationExecutor<Command> {
}
