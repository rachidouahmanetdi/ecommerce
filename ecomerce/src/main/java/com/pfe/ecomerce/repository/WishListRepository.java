package com.pfe.ecomerce.repository;

import com.pfe.ecomerce.entity.WishList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface WishListRepository extends JpaRepository<WishList, Long>, JpaSpecificationExecutor<WishList>{
}
