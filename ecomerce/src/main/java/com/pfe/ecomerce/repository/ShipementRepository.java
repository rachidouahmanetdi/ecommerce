package com.pfe.ecomerce.repository;

import com.pfe.ecomerce.entity.Shipement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ShipementRepository extends JpaRepository<Shipement, Long>, JpaSpecificationExecutor<Shipement> {
}
