package com.pfe.ecomerce.repository;

import com.pfe.ecomerce.entity.ImageProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ImageProductRepository extends JpaRepository<ImageProduct, Long>, JpaSpecificationExecutor<ImageProduct> {
}
