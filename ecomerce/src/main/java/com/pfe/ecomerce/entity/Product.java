package com.pfe.ecomerce.entity;

import lombok.*;

import javax.persistence.*;

@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "product")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CUST_SEQ")
    @SequenceGenerator(sequenceName = "customer_seq", allocationSize = 1, name = "CUST_SEQ")
    private long idProduct;
    @Column(name = "labelProduct")
    private String labelProduct;
    @Column(name = "shipmentPrice")
    private double shipmentPrice;
    @Column(name = "available")
    private boolean available;
    @Column(name = "quantité")
    private Number quantité;
    @Column(name = "description")
    private String description;
    @ManyToOne
    private Categorie categorie;
}
