package com.pfe.ecomerce.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "command")
public class Command {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CUST_SEQ")
    @SequenceGenerator(sequenceName = "customer_seq", allocationSize = 1, name = "CUST_SEQ")
    private long idCommand;
    @Column(name = "dateCreate")
    private Date dateCreate;
    @Column(name = "methodPayment")
    private String methodPayment;
    @Column(name = "etat")
    private String etat;
    @ManyToOne
    private User user;
    @ManyToOne
    private Product product;
    @ManyToOne
    private Shipement shipement;
}
