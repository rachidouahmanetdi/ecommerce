package com.pfe.ecomerce.entity;

import lombok.*;

import javax.persistence.*;

@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "company")
public class Company {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CUST_SEQ")
    @SequenceGenerator(sequenceName = "customer_seq", allocationSize = 1, name = "CUST_SEQ")
    private long idCompany;
    @Column(name = "nameCompany")
    private String nameCompany;
    @Column(name = "adresseCompany")
    private String adresseCompany;
    @Column(name = "referenceCompany")
    private String referenceCompany;
    @Column(name = "cityCompany")
    private String cityCompany;
    @Column(name = "logoCompany")
    private String logoCompany;
}
