package com.pfe.ecomerce.entity;

import lombok.*;

import javax.persistence.*;

@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "commandline")
public class CommandLine {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CUST_SEQ")
    @SequenceGenerator(sequenceName = "customer_seq", allocationSize = 1, name = "CUST_SEQ")
    private long idLine;
    @Column(name = "qte")
    private Number qte;
    @Column(name = "priceAchat")
    private double priceAchat;
    @ManyToOne
    private Command command;
    @ManyToOne
    private  Product product;

}
