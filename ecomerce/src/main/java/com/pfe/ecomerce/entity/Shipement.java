package com.pfe.ecomerce.entity;


import lombok.*;

import javax.persistence.*;

@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "shipement")
public class Shipement {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CUST_SEQ")
    @SequenceGenerator(sequenceName = "customer_seq", allocationSize = 1, name = "CUST_SEQ")
    private Long idShipement;
    @Column(name = "moyenne")
    private String moyenne;
    @Column(name = "numSuivi")
    private String numSuivi;
    @Column(name = "etat")
    private String etat;

}
