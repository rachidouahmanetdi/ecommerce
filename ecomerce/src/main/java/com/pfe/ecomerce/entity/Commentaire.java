package com.pfe.ecomerce.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "commentaire")
public class Commentaire {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CUST_SEQ")
    @SequenceGenerator(sequenceName = "customer_seq", allocationSize = 1, name = "CUST_SEQ")
    private long idComment;
    @Column(name = "commentaire")
    private String commentaire;
    @Column(name = "datePub")
    private Date datePub;
    @ManyToOne
    private Product product;
    @ManyToOne
    private Command command;
}
