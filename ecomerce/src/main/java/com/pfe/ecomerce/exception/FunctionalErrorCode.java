package com.pfe.ecomerce.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

import static java.lang.String.format;

@Getter
public enum FunctionalErrorCode {


    BAD_REQUEST(0, HttpStatus.BAD_REQUEST, "Bad Request"),
    NOT_FOUND_ENTITY(1, HttpStatus.NOT_FOUND, "No record of type %s and with id %s is present in the database"),
    USER_NOT_FOUND(2, HttpStatus.NOT_FOUND, "Login inexist : %s"),
    NOT_NULL_ENTITY(3, HttpStatus.BAD_REQUEST, "%s Entity is required."),
    NOT_NULL_FIELD(4, HttpStatus.BAD_REQUEST, "The following field is required: %s"),
    NOT_NULL_FIELDS(5, HttpStatus.BAD_REQUEST, "The following fields are required : %s"),
    INVALID_JWT_TOKEN(6, HttpStatus.UNAUTHORIZED, "Token JWT expired ou invalid"),
    WRONG_PASSWORD(7, HttpStatus.BAD_REQUEST, "Password incorrect"),
    AUTHORIZATION_FAILED(8, HttpStatus.FORBIDDEN, "Authorization failed for access token request."),
    ALREADY_EXISTE_EXCEPTION(9, HttpStatus.BAD_REQUEST,"%s already exist."),
    USER_EMAIL_NOT_FOUND(10, HttpStatus.NOT_FOUND, "Not such a user with email %s exist in database"),
    OUT_OF_STOCK(11, HttpStatus.BAD_REQUEST, "produit n'a pas de stock"),
    WRONG_EMAIL(12, HttpStatus.BAD_REQUEST, "email incorrect"),
    VALUE_MUST_NOT_CHANGED(13, HttpStatus.BAD_REQUEST,"valeur ne peut etre changé : %s"),
    CANNOT_BE_DELETED(14, HttpStatus.BAD_REQUEST, "article ne peut pas etre supprimé."),
 EMAIL_ALREADY_EXISTE_EXCEPTION(15, HttpStatus.BAD_REQUEST,"Email already exist in database."),
;
    private final String code;
    private final HttpStatus httpStatus;
    private final String messageTemplate;

    FunctionalErrorCode(int code, HttpStatus httpStatus, String messageTemplate) {
        this.code = format("%03d", code);
        this.httpStatus = httpStatus;
        this.messageTemplate = messageTemplate;
    }




}
