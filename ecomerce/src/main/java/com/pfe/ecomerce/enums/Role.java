package com.pfe.ecomerce.enums;

public enum Role {
    ROLE_ADMIN,
    ROLE_USER
}
