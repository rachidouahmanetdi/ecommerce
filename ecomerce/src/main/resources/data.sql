INSERT INTO `dbuser`(`adresse`, `email`, `first_name`, `last_name`, `password`, `phone`, `role`)
VALUES ('casablanca','admin@ecom.com','admin','{bcrypt}$2a$10$1JHZFEbQx.Ee1Kxxvxnf3e.D9pWhcSHBxZIazFlX8i6nHu5YTFm1i','12345',NULL,'ROLE_ADMIN'),
 ('rabat','user@ecom.com','user','user','{bcrypt}$2a$10$1JHZFEbQx.Ee1Kxxvxnf3e.D9pWhcSHBxZIazFlX8i6nHu5YTFm1i',NULL,'ROLE_USER');